#!/usr/bin/env python3
# coding: utf8
#
# Author:  Matej Sujan <sumatej@gmail.com>
# Created: 2024-12-13
# License: BSD
#
# Analyze closed positions and cash operations exported from XTB
#

import csv
# import json
import os
import sys
import yaml
from datetime import datetime, timedelta


class XTBStatement(object):
    def __init__(self, statement_file, tickers_file='tickers.yaml'):
        self.statement = []
        self.tickers = yaml.safe_load(open(tickers_file))['tickers']
        self.cash_operations = False
        self.closed_positions = False
        self.time_format = '%d.%m.%Y %H:%M:%S'
        for operation in csv.DictReader(open(statement_file), delimiter=';'):
            if len(operation) == 0:
                continue
            if not self.cash_operations and not self.closed_positions:
                if 'ID' in operation:
                    self.cash_operations = True
                elif 'Position' in operation:
                    self.closed_positions = True
            self.statement.append(operation)

    def get_operation_types(self):
        op_types = []
        for operation in self.statement:
            if 'ID' in operation:
                """ cash operations """
                if operation['Type'] not in op_types:
                    op_types.append(operation['Type'])
            elif 'Position' in operation:
                """ closed positions """
                if operation['Type'] not in op_types:
                    op_types.append(operation['Type'])
            else:
                print("DEBUG: Unknown operation", str(operation))
                continue
        return op_types

    def get_symbols(self):
        symbols = []
        for operation in self.statement:
            if 'Symbol' in operation:
                """ cash operations """
                if operation['Symbol'] not in symbols:
                    symbols.append(operation['Symbol'])
            else:
                print("DEBUG: Unknown operation", str(operation))
                continue
        return symbols

    def get_operations(self, ticker=None):
        operations = []
        if self.closed_positions and ticker in self.tickers:
            ticker = self.tickers[ticker]
        for operation in self.statement:
            if ticker is None or operation['Symbol'] == ticker:
                operations.append(operation)
        return operations

    def get_operations_summary(self, ticker=None):
        summary = {}
        if self.closed_positions and ticker in self.tickers:
            ticker = self.tickers[ticker]
        for operation in self.get_operations(ticker):
            if self.cash_operations:
                if operation['Type'] not in summary:
                    summary[operation['Type']] = 0
                summary[operation['Type']] += int(float(operation['Amount']) * 100)
            elif self.closed_positions:
                open_time = datetime.strptime(operation['Open time'], self.time_format)
                close_time = datetime.strptime(operation['Close time'], self.time_format)
                if float(operation['Profit']) > 0:
                    param = 'profit'
                else:
                    param = 'loss'
                if close_time - open_time > timedelta(days=365):
                    param += ' older than a year'
                if param not in summary:
                    summary[param] = 0
                summary[param] += int(float(operation['Profit']) * 100)
        total = 0
        for op_type, amount in summary.items():
            total += amount
        summary['total'] = total
        return summary


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('error: No file specified')
        sys.exit(1)

    for fn in sys.argv[1:]:
        statement = XTBStatement(fn)
        for operation in statement.get_operations(os.getenv('TICKER')):
            # print(json.dumps(operation, indent=4))
            print(operation)
        for name, amount in statement.get_operations_summary(os.getenv('TICKER')).items():
            print(name, amount/100)
        # for op_type in statement.get_operation_types():
        #     print(op_type)
        # for symbol in statement.get_symbols():
        #     print(symbol)
