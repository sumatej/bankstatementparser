# coding: utf8
#
# Author:  Matej Sujan <sumatej@gmail.com>
# Created: 2020-08-25
# License: BSD
#
# Parse statement from mBank and convert it to QIF.
#
# Quicken interchange format (QIF) transaction format:
#   D 	Date
#   T 	Amount
#   C 	Cleared status
#   N 	Num (check or reference number)
#   P 	Payee
#   M 	Memo
#   A 	Address (up to five lines; the sixth line is an optional message)
#   L 	Category (Category/Subcategory/Transfer/Class)
#   S 	Category in split (Category/Transfer/Class)
#   E 	Memo in split
#   $ 	Dollar amount of split
#   ^ 	End of the entry
#
# More details about QIF format: https://www.w3.org/2000/10/swap/pim/qif-doc/QIF-doc.htm
#

from bs4 import BeautifulSoup
import glob
import os
import sys
import unicodedata
import yaml

rules_file = 'rules.yaml'
with open(os.path.join(os.path.dirname(sys.argv[0]), rules_file), encoding="utf-8") as fp:
    rules = yaml.safe_load(fp)
recipients_account = rules.get('category', {})
known_recipients = rules.get('account_name', {})
known_account_number = rules.get('account_number', {})
known_recipients_partial = rules.get('transaction_pattern', {})
transaction_types = rules.get('transaction_type', {})
ignored_descriptions = rules.get('ignored_descriptions', [])

if len(sys.argv) == 1:
    print('error: No directory with statements specified')
    sys.exit(1)

bank_name = sys.argv[1]
file_pattern = sys.argv[2]
transactions = []

for fn in glob.glob(file_pattern):
    if not fn.lower().endswith('.htm'): continue
    #print("DEBUG: "+ fn)
    fp = open(fn)
    lines = fp.readlines()
    fp.close()

    lines = unicodedata.normalize('NFKD', '\n'.join(lines)).encode('ascii', 'ignore')
    soup = BeautifulSoup(lines, features="html.parser")
    tables = soup.find_all('table')
        
    statement = []
    rows = tables[-2].find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.get_text('|', True) for ele in cols]
        if cols[0].split('|')[0] in ["C.", "HISTORIA TRANSAKCII KARTY"]:
            continue
        if cols[0] == 'Konecny zostatok:':
            break
        statement.append([ele for ele in cols if ele]) # Get rid of empty values
        del statement[-1][0]
        #print("DEBUG: " + '^'.join(statement[-1]))
    for rec in statement:
        #print("DEBUG:" + '|'.join(rec))
        d, m, y = rec[0].split('|')[1].split('.')
        date = "{}-{}-{}".format(y, m, d)
        payee = ''
        desc = rec[1].split('|')
        memo = ' '.join(desc[1:])
        memo = memo.strip()
        if desc[0].strip() not in ignored_descriptions:
            memo = (desc[0].strip() + ' ' + memo).strip()
        if memo.strip() in known_recipients:
            memo = known_recipients[memo]
        else:
            for key in known_recipients_partial:
                if memo.find(key) != -1:
                    memo = known_recipients_partial[key]
                    break
        if memo in recipients_account:
            payee = recipients_account[memo]
        amount = rec[3].replace(' EUR', '').strip().replace(' ', '')
        number = ''
        t = [date, payee, memo, amount, number]
        print(','.join(t))
        transactions.append(t)

fp = open('mbankstatement.qif', 'w', encoding="utf-8")
fp.write('!Account\nN%s\nTBank\n^\n!Type:Bank\n' % (sys.argv[1]))
for t in transactions:
    date = t[0]
    payee = t[1]
    if payee.split(':')[0] in ['Transfer', 'Payment']:
        payee_type = 'P'
        memo = ''
    else:
        payee_type = 'L'
        memo = t[2]
    payee_type = 'P'
    amount = t[3]
    fp.write('D{}\n{}{}\nCR\nM{}\nT{}\n'.format(date, payee_type, payee, memo, amount))
    if t[4] != '':
        fp.write('N%s\n' % (t[4]))
    fp.write('^\n')	
fp.close()