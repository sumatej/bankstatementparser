# coding: utf8
#
# Author:  Matej Sujan <sumatej@gmail.com>
# Created: 2020-08-25
# License: BSD
#
# Parse statement from SLSP and convert it to QIF.
#
# Quicken interchange format (QIF) transaction format:
#   D 	Date
#   T 	Amount
#   C 	Cleared status
#   N 	Num (check or reference number)
#   P 	Payee
#   M 	Memo
#   A 	Address (up to five lines; the sixth line is an optional message)
#   L 	Category (Category/Subcategory/Transfer/Class)
#   S 	Category in split (Category/Transfer/Class)
#   E 	Memo in split
#   $ 	Dollar amount of split
#   ^ 	End of the entry
#
# More details about QIF format: https://www.w3.org/2000/10/swap/pim/qif-doc/QIF-doc.htm
#

import csv
import glob
import os
import sys
import yaml


if len(sys.argv) == 1:
    print('error: No directory with statements specified')
    sys.exit(1)

bank_name = sys.argv[1]
file_pattern = sys.argv[2]
rules_file = 'rules.yaml'

with open(os.path.join(os.path.dirname(sys.argv[0]), rules_file), encoding="utf-8") as fp:
    rules = yaml.safe_load(fp)
recipients_account = rules.get('category', {})
known_recipients = rules.get('account_name', {})
known_account_number = rules.get('account_number', {})
known_recipients_partial = rules.get('transaction_pattern', {})
transaction_types = rules.get('transaction_type', {})
ignored_descriptions = rules.get('ignored_descriptions', [])

transactions = []

for fn in glob.glob(file_pattern):    
    #print("DEBUG: "+ fn)
    fp = open(fn, newline='', encoding="utf-16")
    spamreader = csv.reader(fp, delimiter='\t', quotechar='"')   
    
    cols_descriptions = []
    for row in spamreader:
        if len(row) == 0: continue
        if row[0] == 'Dátum splatnosti':
            cols_descriptions = row
            continue
        if len(row) != len(cols_descriptions):
            continue
        t = dict(zip(cols_descriptions, row))
        d, m, y = t['Dátum splatnosti'].split('.')
        date = "{}-{}-{}".format(y, m, d)
        payee = ''
        symbols = []
        symbols_internal = {
            'VS': 'Variabilný symbol',
            'ŠS': 'Špecifický symbol',
            'KS': 'Konštantný symbol',
        }
        for sym in ['VS', 'ŠS', 'KS']:
            if t[symbols_internal[sym]].strip() != '':
                symbols.append('{}:{}'.format(sym, t[symbols_internal[sym]]))
        t_type = t['Typ transakcie']
        t_description = t['Poznámka']
        while t_description.find('  ') != -1:
            t_description = t_description.replace('  ', ' ')
        account_name = t['Partner']
        account_number = t['IBAN partnera']
        if account_number == "''":
            account_number = ''
        if account_name == '' and account_number != '':
            account_name = account_number
        desc = [t_type, t_description, account_name, ' '.join(symbols)]
        memo = ' '.join(desc[1:])
        memo = memo.strip()
        if desc[0].strip() not in ignored_descriptions:
            memo = (desc[0].strip() + ' ' + memo).strip()
        if t_type in transaction_types:
            memo = transaction_types[t_type]
        elif account_name in known_recipients:
            memo = known_recipients[account_name]
        elif account_number in known_account_number:
            memo = known_account_number[account_number]
        else:
            for key in known_recipients_partial:
                if memo.find(key) != -1:
                    memo = known_recipients_partial[key]                
                    break
        if memo in recipients_account:
            payee = recipients_account[memo]
        amount = ''.join(t['Suma'].split())
        number = ''
        if float(amount.replace(',', '.')) == 0.0: continue
        tran = [date, payee, memo, amount, number]
        #print(','.join(tran))
        transactions.append(tran)
    fp.close()

fp = open('slspstatement.qif', 'w', encoding="utf-8")
fp.write('!Account\nN%s\nTBank\n^\n!Type:Bank\n' % (sys.argv[1]))
for t in transactions:
    date, payee, memo, amount, number = t
    if payee.split(':')[0] in ['Transfer', 'Payment']:
        payee_type = 'P'
        memo = ''
    else:
        payee_type = 'L'
    # next line is a fix for YNAB which is not correctly handling L (category)
    payee_type = 'P'
    print('|'.join([date, payee, memo, amount, number]))
    fp.write('D{}\n{}{}\nCR\nM{}\nT{}\n'.format(date, payee_type, payee, memo, amount))
    if t[4] != '':
        fp.write('N%s\n' % (t[4]))
    fp.write('^\n')	
fp.close()