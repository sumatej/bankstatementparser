# coding: utf8
#
# Author:  Matej Sujan <sumatej@gmail.com>
# Created: 2020-08-25
# License: BSD
#
# Parse statement from SLSP and convert it to QIF.
#
# Quicken interchange format (QIF) transaction format:
#   D 	Date
#   T 	Amount
#   C 	Cleared status
#   N 	Num (check or reference number)
#   P 	Payee
#   M 	Memo
#   A 	Address (up to five lines; the sixth line is an optional message)
#   L 	Category (Category/Subcategory/Transfer/Class)
#   S 	Category in split (Category/Transfer/Class)
#   E 	Memo in split
#   $ 	Dollar amount of split
#   ^ 	End of the entry
#
# More details about QIF format:
#   https://www.w3.org/2000/10/swap/pim/qif-doc/QIF-doc.htm
#

import csv
import glob
import os
import sys
import yaml

bank_cols_names_conversions = {
    'default': {
        "date": 'date',
        "VS": 'VS',
        "SS": 'SS',
        "KS": 'KS',
        "type": 'type',
        "note": 'note',
        "account_name": 'account_name',
        "account_number": 'account_number',
        "amount": 'amount',
    },
    'ČSOB': {
        "date": 'datum zauctovania',
        "VS": 'referencia platitela',
        "SS": 'referencia platitela',
        "KS": 'referencia platitela',
        "type": 'typ transakcie',
        "note": 'informacia pre prijemcu',
        "account_name": 'nazov protistrany',
        "account_number": 'cislo uctu protistrany',
        "amount": 'suma',
    },
    'mBank': {
        "date": 'Dátum zaúčtovania transakcie',
        "VS": 'VS',
        "SS": 'ŠS',
        "KS": 'KS',
        "type": 'Popis operácie',
        "note": 'Popis',
        "account_name": 'Platca/Príjemca',
        "account_number": 'Číslo účtu platcu/príjemcu',
        "amount": 'Suma transakcie',
    },
    'SLSP': {
        "date": 'Dátum splatnosti',
        "VS": 'Variabilný symbol',
        "SS": 'Špecifický symbol',
        "KS": 'Konštantný symbol',
        "type": 'Typ transakcie',
        "note": 'Poznámka pre príjemcu',
        "account_name": 'Partner',
        "account_number": 'IBAN partnera',
        "amount": 'Suma',
    },
    'UniCredit': {
        "date": 'Dátum hodnoty',
        "VS": 'Variabilný kód',
        "SS": 'Špecifický kód',
        "KS": 'Konštantný kód',
        "type": 'Detaily transakcie 5',
        "note": 'Detaily transakcie 2',
        "account_name": 'Príjemca',
        "account_number": 'Číslo účtu protistrany',
        "amount": 'Suma',
    },
}
necessary_columns = ["date", "VS", "SS", "KS", "type",
                     "note", "account_name", "account_number", "amount"]


def open_file(filename, encoding=None):
    try:
        fp = open(filename, newline='', encoding=encoding)
        line = fp.readline()
    except UnicodeError:
        fp.close()
        return [None, None]
    return [fp, line.strip()]


def write_statement(account, transactions):
    fp = open('statement.qif', 'w', encoding="utf-8")
    fp.write('!Account\nN{}\nTBank\n^\n!Type:Bank\n'.format(account))
    for tran in transactions:
        # next line is a fix for YNAB which is not correctly handling L (category)
        tran['payee_type'] = 'P'
        print("{date}|{payee}|{memo}|{amount}|{number}".format(**tran))
        fp.write('D{date}\n{payee_type}{payee}\nCR\nM{memo}\nT{amount}\n'.format(**tran))
        if tran['number'] != '':
            fp.write('N{number}\n'.format(**tran))
        fp.write('^\n')
    fp.close()


def get_rules(rules):
    result = {}
    for pattern, answer in rules.items():
        result[pattern.upper()] = answer
    return result

def get_symbol(value, group):
    if '/' in value:
        arr = value.split('/')
        for sym in arr:
            if sym.startswith(group):
                rsp = group[2:]
                break
    else:
        rsp = value
    return rsp

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('error: No file/directory with statement(s) specified')
        sys.exit(1)

    file_pattern = sys.argv[1]
    rules_file = 'rules.yaml'

    with open(os.path.join(os.path.dirname(sys.argv[0]), rules_file),
              encoding="utf-8") as fp:
        rules = yaml.safe_load(fp)
    recipients_account = rules.get('category', {})
    known_recipients = get_rules(rules.get('account_name', {}))
    known_account_number = get_rules(rules.get('account_number', {}))
    known_recipients_partial = get_rules(rules.get('transaction_pattern', {}))
    transaction_types = get_rules(rules.get('transaction_type', {}))
    ignored_descriptions = rules.get('ignored_descriptions', [])
    account = ''
    transactions = []
    for fn in glob.glob(file_pattern):
        # print("DEBUG: "+ fn)
        fp, line = open_file(fn, "utf-16")
        if fp is None:
            fp, line = open_file(fn, "utf-8")
        if fp is None:
            fp, line = open_file(fn, "cp1250")

        bank_cols_names = []
        statement_parser = 'csv'
        csv_delimiter = ';'
        if line == 'mBank S.A., pobočka zahraničnej banky;':
            account = 'mBank'
            relevant_columns = bank_cols_names_conversions[account]
            while not line.startswith("#Dátum zaúčtovania transakcie;"):
                line = fp.readline().strip()
                if line[:-1] in known_account_number:
                    account = known_account_number[line[:-1]]
            bank_cols_names = [val[1:] for val in line.split(csv_delimiter)]
        elif line.startswith('"Dátum splatnosti"'):
            account = 'SLSP'
            csv_delimiter = '\t'
            relevant_columns = bank_cols_names_conversions[account]
            bank_cols_names = [val[1:-1] for val in line.strip().split(csv_delimiter)]
        elif line.find('Zoznam transakcií účtu;') != -1:
            account = 'UniCredit'
            relevant_columns = bank_cols_names_conversions[account]
            for n in range(1, 4):
                line = fp.readline()
            bank_cols_names = line.strip().split(csv_delimiter)
        elif line.find('Pohyby na ucte c. ') != -1:
            account = 'ČSOB'
            csv_delimiter = ','
            relevant_columns = bank_cols_names_conversions[account]
            for n in range(1, 3):
                line = fp.readline()
            bank_cols_names = line.strip().split(csv_delimiter)
        elif line == '<html>':
            statement_parser = 'html'
            account = 'mBank mKreditka'
            csv_delimiter = None
            relevant_columns = bank_cols_names_conversions['default']
            bank_cols_names = necessary_columns
        elif line == 'Suma v originálnej mene':
            statement_parser = 'text'
            account = 'mBank mKreditka'
            csv_delimiter = None
            relevant_columns = bank_cols_names_conversions['default']
            bank_cols_names = necessary_columns
        else:
            print("error: Unrecognized statement format")
            sys.exit(1)

        mortgage_reported = {}
        if statement_parser == 'csv' and csv_delimiter is not None:
            csv_export = csv.reader(fp, delimiter=csv_delimiter, quotechar='"')
        elif statement_parser == 'html':
            # for mBank Credit Card statement
            from bs4 import BeautifulSoup
            import unicodedata
            csv_export = []
            lines = [line]
            lines.extend(fp.readlines())
            fp.close()

            lines = unicodedata.normalize('NFKD', '\n'.join(lines)).encode(
                'ascii', 'ignore')
            soup = BeautifulSoup(lines, features="html.parser")
            tables = soup.find_all('table')

            statement = []
            rows = tables[-2].find_all('tr')
            for row in rows:
                cols = row.find_all('td')
                cols = [ele.get_text('|', True) for ele in cols]
                if cols[0].split('|')[0] in ["C.", "HISTORIA TRANSAKCII KARTY"]:
                    continue
                if cols[0] == 'Konecny zostatok:':
                    break
                # Get rid of empty values
                statement.append([ele for ele in cols if ele])
                del statement[-1][0]
                # print("DEBUG: " + '^'.join(statement[-1]))
            for rec in statement:
                # print("DEBUG:" + '|'.join(rec))
                d, m, y = rec[0].split('|')[1].split('.')
                date = "{}-{}-{}".format(y, m, d)
                desc = rec[1].split('|')
                type = desc[0].strip()
                note = ' '.join(desc[1:])
                note = note.strip()
                amount = rec[3].replace(' EUR', '').strip().replace(' ', '')
                t = [date, "", "", "", type, note, "", "", amount]
                csv_export.append(t)
        elif statement_parser == 'text':
            csv_export = []
            lines = [line]
            lines.extend(fp.readlines())
            fp.close()
            conv_table = {
                'Dátum zaúčtovania': "date",
                'Typ platby': "type",
                'Miesto transakcie': "note",
                'Suma v mene účtu': "amount",
            }
            params = {}
            par = val = None
            for data in lines:
                data = data.strip()
                if data == '':
                    if params.get('amount') is not None and params.get('type') is None:
                        continue
                    par = val = None
                    if len(params) == 0:
                        continue
                    t = [
                        params['date'],
                        "",
                        "",
                        "",
                        params['type'],
                        params.get('note', ''),
                        "",
                        "",
                        params['amount'],
                    ]
                    csv_export.append(t)
                    params = {}
                    continue
                if par is None:
                    par = conv_table.get(data, data)
                elif val is None:
                    if par == 'amount':
                        val = data.replace(' EUR', '').strip().replace(' ', '')
                    else:
                        val = data
                    params[par] = val
                else:
                    par = conv_table.get(data, data)
                    val = None
            if len(params) > 0:
                t = [
                    params['date'],
                    "",
                    "",
                    "",
                    params['type'],
                    params.get('note', ''),
                    "",
                    "",
                    params['amount'],
                ]
                csv_export.append(t)
        else:
            print("error: Unsupported statement parser parameters")
            sys.exit(1)

        for row in csv_export:
            if len(row) == 0 or len(row) != len(bank_cols_names):
                continue
            bank_transaction = dict(zip(bank_cols_names, row))
            t = {}
            for par in necessary_columns:
                t[par] = bank_transaction[relevant_columns[par]]
            date = t['date']
            # in case a date is reported as 16.12.2018 convert it to 2018-12-16
            d, m, y = date.replace('.', '-').split('-')
            if int(y) > 31:
                date = "{}-{:02d}-{:02d}".format(y, int(m.strip()), int(d.strip()))
            payee = ''
            symbols = []
            for sym in ['VS', 'SS', 'KS']:                
                if t[sym].strip() != '':
                    value = get_symbol(t[sym].strip(), sym)
                    if value != '':
                        symbols.append('{}:{}'.format(sym, str(int(value))))
            t_type = t['type'].strip()
            t_description = t['note']
            while t_description.find('  ') != -1:
                t_description = t_description.replace('  ', ' ')
            account_name = t['account_name']
            account_number = t['account_number']
            if "'" in account_number:
                account_number = account_number.split("'")[1]
            if account_name in ['', 'NOT PROVIDED'] and account_number != '':
                account_name = account_number
            desc = [t_description, account_name, ' '.join(symbols)]
            if t_type not in ignored_descriptions and t_type.strip() != '':
                desc.append(t_type)
            memo = ' '.join(desc).strip()
            if t_type.upper() in transaction_types:
                memo = transaction_types[t_type.upper()]
            elif account_name.upper() in known_recipients:
                memo = known_recipients[account_name.upper()]
            elif account_number.upper() in known_account_number:
                memo = known_account_number[account_number.upper()]
            else:
                for key in known_recipients_partial:
                    if memo.upper().find(key) != -1:
                        memo = known_recipients_partial[key]
                        break
            amount = ''.join(t['amount'].strip().split())
            number = ''
            if float(amount.replace(',', '.')) == 0.0:
                continue
            if memo.startswith('mortgage:'):
                my_mortgage = memo.split(':')[1]
                my_mortgage_payment = my_mortgage + ':' + date
                my_amount = float(amount.replace(',', '.').replace('-', ''))
                if my_mortgage_payment not in mortgage_reported:
                    mortgage_reported[my_mortgage_payment] = []
                for rule, details in rules['mortgage'][my_mortgage].items():
                    if rule in mortgage_reported[my_mortgage_payment]:
                        continue
                    if ('amount' in details and
                            my_amount == float(details['amount'])):
                        memo = details['memo']
                        mortgage_reported[my_mortgage_payment].append(rule)
                        break
                    elif ('amount_less_than' in details and
                            my_amount < float(details['amount_less_than'])):
                        memo = details['memo']
                        mortgage_reported[my_mortgage_payment].append(rule)
                        break
                    elif ('amount_more_than' in details and
                            my_amount > float(details['amount_more_than'])):
                        memo = details['memo']
                        mortgage_reported[my_mortgage_payment].append(rule)
                        break
            if memo in recipients_account:
                payee = recipients_account[memo]
            tran = {
                "date": date,
                "payee": payee,
                "memo": memo,
                "amount": amount,
                "number": number,
            }
            transactions.append(tran)
        fp.close()

write_statement(account, transactions)
