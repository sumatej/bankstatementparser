# coding: utf8
#
# Author:  Matej Sujan <sumatej@gmail.com>
# Created: 2020-08-25
# License: BSD
#
# Parse statement from mBank and convert it to QIF.
#
# Quicken interchange format (QIF) transaction format:
#   D 	Date
#   T 	Amount
#   C 	Cleared status
#   N 	Num (check or reference number)
#   P 	Payee
#   M 	Memo
#   A 	Address (up to five lines; the sixth line is an optional message)
#   L 	Category (Category/Subcategory/Transfer/Class)
#   S 	Category in split (Category/Transfer/Class)
#   E 	Memo in split
#   $ 	Dollar amount of split
#   ^ 	End of the entry
#
# More details about QIF format: https://www.w3.org/2000/10/swap/pim/qif-doc/QIF-doc.htm
#

from bs4 import BeautifulSoup
import glob
import os
import sys
import unicodedata

recipients_account = {
    '4ka': 'Výdavky: Telefonovanie a online služby',
    'AboutYou': 'Výdavky: Oblečenie',
    'Netflix': 'Výdavky: Telefonovanie a online služby',
    'Palivo': 'Nevyhnutné výdavky: Doprava',
    'SLSP Bezny ucet': 'Transfer: SLSP Bezny ucet',
    'Tesco': 'Nevyhnutné výdavky: Stravovanie',
    'bonus': 'Inflow: To be Budgeted',
    'dan z urokov': 'Výdavky: Ostatné bez rozpočtu',
    'mBank eMax': 'Transfer: mBank eMax',
    'mBank mKonto': 'Transfer: mBank mKonto',
    'mBank mSporenie': 'Transfer: mBank mSporenie',
    'splatenie kreditnej karty': 'Payment: mBank mKreditka',
    'uroky': 'Inflow: To be Budgeted',
    'vyber v bankomate': 'Transfer: Hotovosť',
}

known_recipients = {
    'BONUS BONUS MKREDITKA PLUS': 'bonus',
    'DAN Z KAPITALIZACIE UROKOV': 'dan z urokov',
    'KAPITALIZACIA UROKOV': 'uroky',
    'PAYPAL *ABOUTYOUGMB': 'AboutYou',
}

known_recipients_partial = {
    'AUTOMATICKE SPLATENIE KARTY': 'splatenie kreditnej karty',
    'DE88500700100175526303': 'Netflix',
    'MANUALNE SPLATENIE KREDITNEJ KARTY': 'splatenie kreditnej karty',
    'NETFLIX COM': 'Netflix',
    'PREVOD NA MSPORENIE': 'mBank mSporenie',
    'SK0783605207004200118669': 'mBank mKonto',
    'SK0783605207004205655332': 'mBank mSporenie',
    'SK1983605207004202037889': 'mBank eMax',
    'SK2483605207004202032658': 'mBank eMax',
    'SK6109000000000179023172': 'SLSP Bezny ucet',
    'TAM FEROVE PUMPY': 'Palivo',
    'TESCO': 'Tesco',
    'VYBER V BANKOMATE': 'vyber v bankomate',    
    'WWW.4KA.SK': '4ka',
}

ignored_descriptions = [
    'PLATBA KARTOU',
    'PLATBA KARTOU V ZAHRANICI',
    'PLATBA CEZ INTERNET',
]


if len(sys.argv) == 1:
    print('error: No directory with statements specified')
    sys.exit(1)

bank_name = sys.argv[1]
file_pattern = sys.argv[2]
transactions = []

for fn in glob.glob(file_pattern):    
    if not fn.endswith('.htm'): continue
    #print("DEBUG: "+ fn)
    fp = open(fn)
    lines = fp.readlines()
    fp.close()

    lines = unicodedata.normalize('NFKD', '\n'.join(lines)).encode('ascii', 'ignore')
    soup = BeautifulSoup(lines, features="html.parser")    
    table = soup.find('table', attrs={'align':'left'})
    if table is None:
        continue
    statement = []
    rows = table.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.get_text('|', True) for ele in cols]
        if cols[0] == "Uskutocnene|Zauctovane": continue
        statement.append([ele for ele in cols if ele]) # Get rid of empty values        
    for rec in statement:
        if len(rec) < 3: continue
        #print("DEBUG:" + '|'.join(rec))
        d, m, y = rec[0].split('|')[1].split('.')
        date = "{}-{}-{}".format(y, m, d)
        payee = ''
        desc = rec[1].split('|')
        memo = ' '.join(desc[1:])
        memo = memo.strip()
        if desc[0].strip() not in ignored_descriptions:
            memo = (desc[0].strip() + ' ' + memo).strip()
        if memo.strip() in known_recipients:
            memo = known_recipients[memo]
        else:
            for key in known_recipients_partial:
                if memo.find(key) != -1:
                    memo = known_recipients_partial[key]                
                    break
        if memo in recipients_account:
            payee = recipients_account[memo]
        amount = rec[2].replace(' EUR', '').strip()
        number = ''
        t = [date, payee, memo, amount, number]
        print(','.join(t))
        transactions.append(t)

fp = open('mbankstatement.qif', 'w', encoding="utf-8")
fp.write('!Account\nN%s\nTBank\n^\n!Type:Bank\n' % (sys.argv[1]))
for t in transactions:
    date = t[0]
    payee = t[1]
    if payee.split(':')[0] in ['Transfer', 'Payment']:
        payee_type = 'P'
        memo = ''
    else:
        payee_type = 'L'
        memo = t[2]
    payee_type = 'P'
    amount = t[3]
    fp.write('D{}\n{}{}\nCR\nM{}\nT{}\n'.format(date, payee_type, payee, memo, amount))
    if t[4] != '':
        fp.write('N%s\n' % (t[4]))
    fp.write('^\n')	
fp.close()