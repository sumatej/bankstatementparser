# coding: utf8
#
# Author:  Matej Sujan <sumatej@gmail.com>
# Created: 2020-08-26
# License: BSD
#
# Parse statement from Slovenska Sporitelna (SLSP) and convert it to QIF.
#
# Quicken interchange format (QIF) transaction format:
#   D     Date
#   T     Amount
#   C     Cleared status
#   N     Num (check or reference number)
#   P     Payee
#   M     Memo
#   A     Address (up to five lines; the sixth line is an optional message)
#   L     Category (Category/Subcategory/Transfer/Class)
#   S     Category in split (Category/Transfer/Class)
#   E     Memo in split
#   $     Dollar amount of split
#   ^     End of the entry
#
# More details about QIF format: https://www.w3.org/2000/10/swap/pim/qif-doc/QIF-doc.htm
#

import glob
import os
import subprocess
import sys

pdftotext = 'c:\\Tools\\xpdf-tools-win\\bin64\\pdftotext.exe '
tmpfile = 'tmpfile.txt'

if len(sys.argv) == 1:
    print('error: No directory with statements specified')
    sys.exit(1)

recipients_account = {
    "McDonald's": 'Nevyhnutné výdavky: Stravovanie',
    '101 drogerie': 'Nevyhnutné výdavky: Potraviny a drogéria',
    '4ka': 'Výdavky: Telefonovanie a online služby',
    'AboutYou': 'Výdavky: Oblečenie',
    'AliExpress': '',
    'AllToys': 'Výdavky: Darčeky',
    'Alza': 'Výdavky: Ostatné bez rozpočtu',
    'Billa': 'Nevyhnutné výdavky: Stravovanie',
    'BonPrix': 'Výdavky: Oblečenie',
    'C&A': 'Výdavky: Oblečenie',
    'DM': 'Nevyhnutné výdavky: Potraviny a drogéria',
    'Decathlon': 'Výdavky: Oblečenie',
    'Deichmann': 'Výdavky: Oblečenie',
    'Dracik': 'Výdavky: Darčeky',
    'Elenka, atletika': 'Quality of Life Goals: Fitness',
    'Epic Games': 'Just for Fun: Fun Money Marek',
    'GAP poistenie': 'Výdavky: Poistenie',
    'GearBest': '',
    'GoPay': '',
    'H&M': 'Výdavky: Oblečenie',
    'Hornbach': 'Výdavky: Domácnosť',
    'IKEA': 'Výdavky: Domácnosť',
    'Kaufland': 'Nevyhnutné výdavky: Stravovanie',
    'Lidl': 'Nevyhnutné výdavky: Stravovanie',
    'Lidl-shop': 'Výdavky: Ostatné bez rozpočtu',
    'Marek, doucovanie': 'Quality of Life Goals: Education',
    'Marek, skauting': 'Quality of Life Goals: Fitness',
    'Matej, AXA Kumulativ MAX': 'Výdavky: Investície',
    'Michaela Fajkusova': '',
    'Netflix': 'Výdavky: Telefonovanie a online služby',
    'Nokia': 'Inflow: To be Budgeted',
    'O2': 'Výdavky: Telefonovanie a online služby',
    'OBI': 'Výdavky: Domácnosť',
    'PZP a havarijne poistenie': 'Výdavky: Poistenie',
    'PZP': 'Výdavky: Poistenie',
    'PayPal': '',
    'ProCare': 'Výdavky: Zdravie',
    'Protetika': 'Výdavky: Oblečenie',
    'SLSP Bezny ucet': 'Transfer: SLSP Bezny ucet',
    'SLSP Sporenie': 'Transfer: SLSP Sporenie',
    'SLSP poplatok za vedenie uctu': 'Výdavky: Telefonovanie a online služby',
    'Tesco': 'Nevyhnutné výdavky: Stravovanie',
    'Wonderschool': 'Inflow: To be Budgeted',
    'ZUS Jozefa Kresánka': 'Quality of Life Goals: Education',
    'Zdenka, AXA Kumulativ MAX': 'Výdavky: Investície',
    'Zuzka Hruzova': '',
    'ZŠ Lamač': 'Quality of Life Goals: Education',
    'bonus': 'Inflow: To be Budgeted',
    'cestovne poistenie': 'Výdavky: Poistenie',
    'dan z nehnutelnosti': 'Nevyhnutné výdavky: Bývanie pravidelné',
    'dan z urokov': 'Výdavky: Ostatné bez rozpočtu',
    'dar Fajkusovcom': 'Výdavky: Dobročinnosť',
    'dermatologia': 'Výdavky: Zdravie',
    'detske pridavky': 'Inflow: To be Budgeted',
    'dialnicna znamka': 'Nevyhnutné výdavky: Doprava',
    'elektrina': 'Nevyhnutné výdavky: Bývanie pravidelné',
    'fara Lamac': 'Výdavky: Dobročinnosť',
    'galanteria': 'Výdavky: Oblečenie',
    'havarijne poistenie': 'Výdavky: Poistenie',
    'internet a televizia': 'Nevyhnutné výdavky: Bývanie pravidelné',
    'kancelarske potreby': 'Výdavky: Domácnosť',
    'knihy': 'Quality of Life Goals: Education',
    'koncesionarske poplatky': 'Nevyhnutné výdavky: Bývanie pravidelné',
    'lekaren': 'Výdavky: Zdravie',
    'ludialudom.sk': 'Výdavky: Dobročinnosť',
    'mBank eMax': 'Transfer: mBank eMax',
    'mBank mKonto': 'Transfer: mBank mKonto',
    'mBank mSporenie': 'Transfer: mBank mSporenie',
    'materske': 'Inflow: To be Budgeted',
    'oblecenie': 'Výdavky: Oblečenie',
    'obuv': 'Výdavky: Oblečenie',
    'palivo': 'Nevyhnutné výdavky: Doprava',
    'parkovanie': 'Nevyhnutné výdavky: Doprava',
    'plyn': 'Nevyhnutné výdavky: Bývanie pravidelné',
    'poistenie bytu': 'Výdavky: Poistenie',
    'poistenie domacnosti': 'Výdavky: Poistenie',
    'potraviny': 'Nevyhnutné výdavky: Stravovanie',
    'preplatok': 'Nevyhnutné výdavky: Bývanie pravidelné',
    'pupocnikova krv': 'Výdavky: Zdravie',
    'rehabilitacie': 'Výdavky: Zdravie',
    'restauracia': 'Nevyhnutné výdavky: Stravovanie',
    'rodicovsky prispevok': 'Inflow: To be Budgeted',
    'servis': 'Výdavky: Auto servis',
    'skolska jedalen': 'Nevyhnutné výdavky: Stravovanie',
    'splatenie kreditnej karty': 'Payment: mBank mKreditka',
    'splatka kreditnej karty': 'Payment: mBank mKonto',
    'uroky na kreditnej karte': 'Výdavky: Ostatné bez rozpočtu',
    'uroky': 'Inflow: To be Budgeted',
    'vyber v bankomate': 'Transfer: Hotovosť',
    'zalohova platba': 'Nevyhnutné výdavky: Bývanie pravidelné',
    'zubar': 'Výdavky: Zdravie',
}

known_recipients = {
    'BONUS BONUS MKREDITKA PLUS': 'bonus',
    'Daň z úroku a iných výnosov': 'dan z urokov',
    'Kreditný úrok': 'urok',
    'KAPITALIZACIA UROKOV': 'uroky',
    'UROKY': 'uroky na kreditnej karte',
    'PAYPAL *ABOUTYOUGMB': 'AboutYou',
    'SPLATKA - AUTOMATICKE ZAUCTOVANIE': 'splatka kreditnej karty',
    'SPLATKA - MANUAL ZAUCTOVANIE': 'splatka kreditnej karty',
    'VEDA TURCIANSKA; BRATISLAVA': 'restauracia',
    'lidl-shop.sk; Bratislava': 'Lidl-shop',
    'IKEA,BA,IVANSKA CESTA; BRATISLAVA': 'IKEA',
    'IKEA, BA, SVEDSKY OBCH; BRATISLAVA': 'potraviny',
    'PORSCHE-INTER AUTO DOL; BRATISLAVA': 'servis',
    'RTVS': 'koncesionarske poplatky',
    'Západoslovenská energetika': 'elektrina',
    'ZSE Energia,a.s.': 'elektrina',
    'MC Bratislava Lamac - skolská jedále': 'skolska jedalen',
    'Atletický klub Slávia UK Brati': 'Elenka, atletika',
    'UPC Broadband Slovakia': 'internet a televizia',
    'AG-EXPERT, s.r.o.': 'zalohova platba',
    'NOKIA Slovakia, a.s.': 'Nokia',
    'NOKIA CORPORATION': 'Nokia',
    'SONFOL S.R.O.': 'Wonderschool',
    'Slovenská posta SIPO': 'plyn',
    'Deti a umenie n.f.': 'ZUS Jozefa Kresánka',
    'ZUS Jozefa Kresánka': 'ZUS Jozefa Kresánka',
    'Cord Blood Center AG': 'pupocnikova krv',
    'Hlavne mesto SR Bratislava odd.': 'dan z nehnutelnosti',
    '104. zbor skautov Bratislava Ruzinov': 'Marek, skauting',
}

known_account_number = {
    'SK29 0900 0000 0050 3384 5424': 'Alza',
    'SK92 0900 0000 0006 3450 6775': 'Epic Games',
    'SK74 0900 0000 0051 1911 4307': 'SLSP Sporenie',
    'SK24 8360 5207 0042 0203 2658': 'mBank eMax',
    'SK07 8360 5207 0042 0011 8669': 'mBank mKonto',
    'SK14 0900 0000 0006 3096 4963': 'O2',
    'SK31 1100 0000 0026 1773 7685': 'Zuzka Hruzova',
    'disabled CZ50 0300 0000 0002 1101 2212': 'Michaela Fajkusova',
    'CZ24 0300 0000 0002 1070 1892': 'Martin Fajkus',
    'SK36 0900 0000 0006 3531 0555': 'GoPay',
    'SK74 0200 0000 0026 1588 0453': 'ZŠ Lamač',
    'SK68 0900 0000 0050 7285 8316': '4ka',
    'SK22 0900 0000 0050 2381 4683': 'ludialudom.sk',
    'SK28 0900 0000 0003 5236 3833': 'knihy',
    'SK89 0900 0000 0000 1160 0765': 'knihy',
    'SK58 1111 0000 0010 8825 7007': 'Marek, doucovanie',
    'SK23 0900 0000 0006 3496 0716': 'ZlavaDna',
    'SK40 0900 0000 0051 6504 0345': 'fara Lamac',
    'SK14 0900 0000 0004 5216 9034': 'Elenka, atletika',
}

known_recipients_partial = {
    '101 DROGERIE': '101 drogerie',
    'A3 SPORT': 'obuv',
    'ALIEXPRESS': 'AliExpress',
    'ALLTOYS': 'AllToys',
    'AUTOMATICKE SPLATENIE KARTY': 'splatenie kreditnej karty',
    'AXA pojisovna a.s. VS: 8701601130': 'PZP a havarijne poistenie',
    'AXA zivotní pojisovna a.s. VS: 9599041897': 'Matej, AXA Kumulativ MAX',
    'AXA zivotní pojisovna a.s. VS: 9599041898': 'Zdenka, AXA Kumulativ MAX',
    'BATA': 'obuv',
    'BILLA': 'Billa',
    'BM STORES': 'oblecenie',
    'BONPRIX': 'BonPrix',
    'Bezhotovostný vklad SK26 1100 0000 0026 2971 2522': 'preplatok',
    'C & A': 'C&A',
    'C&A': 'C&A',
    'CALLIOPE': 'oblecenie',
    'CCC': 'oblecenie',
    'COOP': 'potraviny',
    'DE88500700100175526303': 'Netflix',
    'DEKORITA': 'galanteria',
    'DERMAREVOLTA, s.r.o': 'dermatologia',
    'DM': 'DM',
    'DR. MAX': 'lekaren',
    'DR.MAX': 'lekaren',
    'DRACIK': 'Dracik',
    'Decathlon': 'Decathlon',
    'Defend Insurance s.r.o. VS: 122372': 'GAP poistenie',
    'GATE': 'oblecenie',
    'Generali Poisova, a.s. VS: 2403011821': 'poistenie bytu',
    'H&M': 'H&M',
    'HORNBACH': 'Hornbach',
    'KAUFLAND': 'Kaufland',
    'KIK TEXTIL': 'galanteria',
    'KOOPERATIVA poisova, a.s. Vienna Insur VS: 6586775272': 'havarijne poistenie',
    'KOOPERATIVA poisova, a.s. Vienna Insur VS: 6586939417': 'PZP',
    'LEKAREN': 'lekaren',
    'LIDL': 'Lidl',
    'LINDEX': 'oblecenie',
    'Lekaren': 'lekaren',
    'MANUALNE SPLATENIE KREDITNEJ KARTY': 'splatenie kreditnej karty',
    'MARTINUS': 'knihy',
    'MOVEO': 'kancelarske potreby',
    'MUDR JANA STRAKOVA': 'zubar',
    'McDonald': "McDonald's",
    'Michaela Fajkusová POVZBUDENIE': 'dar Fajkusovcom',
    'NETFLIX COM': 'Netflix',
    'OBI': 'OBI',
    'OMV': 'palivo',
    'PARK SABA-DFNSP': 'parkovanie',
    'PEEK CLOPPENBURG': 'oblecenie',
    'PIZZERIA': 'restauracia',
    'PRESKOLY': 'knihy',
    'PRESTO B5': 'restauracia',
    'PREVOD NA MSPORENIE': 'mBank mSporenie',
    'PROCARE': 'ProCare',
    'PROFY, S R O': 'rehabilitacie',
    'PROTETIKA': 'Protetika',
    'Panta Rhei': 'knihy',
    'Poplatok za vedenie úctu': 'SLSP poplatok za vedenie uctu',
    'RESTAURACIA': 'restauracia',
    'SCHNITZEL': 'restauracia',
    'SHELL': 'palivo',
    'SINSAY': 'oblecenie',
    'SK0783605207004200118669': 'mBank mKonto',
    'SK0783605207004205655332': 'mBank mSporenie',
    'SK1983605207004202037889': 'mBank eMax',
    'SK2483605207004202032658': 'mBank eMax',
    'SK61 0900 0000 0050 3151 2386 BIC: GIBASKBX VS: 3900147704': 'poistenie domacnosti',
    'SK6109000000000179023172': 'SLSP Bezny ucet',
    'SLOVENKA': 'oblecenie',
    'SPORTSDIRECT.COM': 'oblecenie',
    'SUPERMERCATO SUPER': 'potraviny',
    'Sevt': 'kancelarske potreby',
    'Slovenka': 'oblecenie',
    'Sociálna poisova VS: 8154086270': 'materske',
    'TAM FEROVE PUMPY': 'palivo',
    'TERNO': 'potraviny',
    'TESCO': 'Tesco',
    'Tibor Varga TSV PAP-Bratislava': 'kancelarske potreby',
    'Union poistovna, a.s. VS: 0050186766': 'cestovne poistenie',
    'Union poistovna, a.s. VS: 50186766': 'cestovne poistenie',
    'VITALAND': 'potraviny',
    'VS: 1902409778': 'O2',
    'VYBER V BANKOMATE': 'vyber v bankomate',    
    'Výber kartou': 'vyber v bankomate',
    'WWW.4KA.SK': '4ka',
    'Zasielkovna': 'Zasielkovna',
    'eznamka': 'dialnicna znamka',
    'Úrad práce, sociálnych vecí a rodiny Bratislava VS: 0781216623': 'detske pridavky',
    'Úrad práce, sociálnych vecí a rodiny Bratislava VS: 8154086270': 'rodicovsky prispevok',
}

transactions = []
for fn in glob.glob(sys.argv[2]):
    if subprocess.call([pdftotext, '-table', '-nopgbrk', '-bom', fn, tmpfile]) != 0:
        print("error: Failed to execute "+pdftotext)
        sys.exit(1)

    try:
        fp = open(tmpfile)
        lines = [line for line in fp.readlines() if line.strip() != '']
        fp.close()
    except Exception:
        print("error: Failed to open file %s (%s)" % (tmpfile, fn))
        sys.exit(1)
    os.unlink(tmpfile)
        
    prev_line = ''
    in_transactions_list = False
    statement = []
    for line in lines:
        if not in_transactions_list:
            if line.lstrip().startswith('valuty'):
                in_transactions_list = True
                left_padding = len(line) - len(line.lstrip())
                continue
            continue
        line = line[left_padding:].strip()
        if line == '': continue
        if line.startswith("info@slsp.sk") or line == 'Poznámka:':
            in_transactions_list = False
            continue
        record = [part.strip() for part in line.split('  ') if part.strip() != '']
        if len(record) > 3:
            if len(record) == 4:
                record.insert(2, prev_line)
                statement[-1][1] = statement[-1][1][:-len(prev_line)]
            statement.append([record[1], record[2], record[3].replace(' ', '').replace(',', '.')])
        else:
            statement[-1][1] += '|' + line
        prev_line = line
    for rec in statement:
        #print("DEBUG: " + '^'.join(rec))
        d, m, y = rec[0].split('.')
        date = "{}-{}-{}".format(y, m, d)
        payee = ''
        desc = rec[1].split('|')
        if desc[0] == 'Platba kartou':
            memo = ' '.join(desc[2:])[:-21]
        else:
            memo = ' '.join(desc[1:])
        account_name = ''
        account_number = ''
        for d in desc:
            if d.startswith('Názov protiúctu:'):
                account_name = d[len('Názov protiúctu:')+1:].strip()
                memo = account_name
                break
        if desc[0] not in ['Poplatok za vedenie úctu', 'Platba kartou']:
            account_number = desc[1].split(' BIC: ')[0]
        memo = memo.strip()
        if account_name in known_recipients:
            memo = known_recipients[account_name]
        elif account_number in known_account_number:
            memo = known_account_number[account_number]
        else:
            for key in known_recipients_partial:
                if ' '.join(desc).find(key) != -1:
                    memo = known_recipients_partial[key]                
                    break
        if memo in recipients_account:
            payee = recipients_account[memo]
        amount = rec[2]
        number = ''
        if float(amount) == 0.0:
            continue
        t = [date, payee, memo, amount, number]
        print(','.join(t))
        transactions.append(t)        

fp = open('slspstatement.qif', 'w', encoding="utf-8")
fp.write('!Account\nN%s\nTBank\n^\n!Type:Bank\n' % (sys.argv[1]))
for t in transactions:
    date = t[0]
    payee = t[1]
    if payee.split(':')[0] in ['Transfer', 'Payment']:
        payee_type = 'P'
        memo = ''
    else:
        payee_type = 'L'
        memo = t[2]
    payee_type = 'P'
    amount = t[3]
    fp.write('D{}\n{}{}\nCR\nM{}\nT{}\n'.format(date, payee_type, payee, memo, amount))
    if t[4] != '':
        fp.write('N%s\n' % (t[4]))
    fp.write('^\n')	
fp.close()

