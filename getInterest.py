from bs4 import BeautifulSoup
import sys, os, unicodedata

if len(sys.argv) == 1:
    print('error: No directory with statements specified')
    sys.exit(1)

srcdir = sys.argv[1]

try:
	fout = open('interest.xls', 'w')
	fout.write('datum\turok\tdan\n')
except:
	print("error: Failed to open output file")
	sys.exit(1)

for fn in os.listdir(srcdir):
	if not fn.endswith('.htm'): continue
	fp = open(os.path.join(srcdir, fn))
	lines = fp.readlines()
	fp.close()

	lines = unicodedata.normalize('NFKD', '\n'.join(lines)).encode('ascii', 'ignore')
	soup = BeautifulSoup(lines)
	data = []
	table = soup.find('table', attrs={'align':'left'})

	rows = table.find_all('tr')
	for row in rows:
		cols = row.find_all('td')
		cols = [ele.text.strip() for ele in cols]
		data.append([ele for ele in cols if ele]) # Get rid of empty values
	udaje = {'datum': '?', 'urok': '0', 'dan': '0'}
	for row in data:
		if len(row) < 3: continue
		if row[1] == 'KAPITALIZACIA UROKOV':
			udaje['datum'] = row[0][10:]
			udaje['urok'] = row[2].split(' ')[0]
		elif row[1] == 'DAO Z KAPITALIZACIE UROKOV':
			udaje['dan'] = row[2].split(' ')[0]
	if udaje['urok'] != '0':	    
		out = '%(datum)s\t%(urok)s\t%(dan)s' % (udaje)
		print(out)
		fout.write(out+'\n')
fout.close()