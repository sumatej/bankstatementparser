# coding: utf8
#
# Parse statement from mBank
#
# Author:  Matej Sujan <sumatej@gmail.com>
# Created: 2020-08-22
# License: BSD
#

import sys

recipients_account = {
	'Palivo': 'Palivo',
    'Tesco': 'Tesco',
}

known_recipients = {
	'PAYPAL *ABOUTYOUGMB': 'AboutYou',
}

known_recipients_partial = {
    'TESCO': 'Tesco',
    'TAM FEROVE PUMPY': 'Palivo',
}

ignored_descriptions = [
    'PLATBA KARTOU',
    'PLATBA KARTOU V ZAHRANIČÍ',
    'PLATBA CEZ INTERNET',
    'SPLÁTKA - MANUÁL ZAÚČTOVANIE',
]

lines = open(sys.argv[1], encoding="utf-8").readlines()
transactions = []
i = 0
while i < len(lines):
    nb, date1, date2, desc1, desc2, amount1, amount2 = lines[i:i+7]
    date = date2.strip()
    payee = ''
    if desc1.strip() == 'SPLÁTKA - MANUÁL ZAÚČTOVANIE':
        payee = 'Payment from: mBank eKonto'
    memo = desc2.strip().split(';')[0]
    if desc1.strip() not in ignored_descriptions:
        memo = desc1.strip() + ' ' + memo
    if memo in known_recipients:
        memo = known_recipients[memo]
    else:
        for key in known_recipients_partial:
            if memo.find(key) != -1:
                memo = recipients_account[known_recipients_partial[key]]
                break
    if amount1.strip() != amount2.strip():
        memo += ' ' + amount1.strip() 
    amount = amount2.strip().replace(' EUR', '').replace(' ', '')
    t = [date, payee, memo, amount]
    print(','.join(t))
    transactions.append(t)
    i += 7
    try:
        if lines[i].strip() == '':
            i += 1
    except IndexError:
        pass

fp = open(sys.argv[1].split('.')[0]+'.csv', 'w')
fp.write('Date,Payee,Memo,Amount\n')
for t in transactions:
	fp.write(','.join(t) + '\n')
#	if t[4] != '':
#		fp.write('N%s\n' % (t[4]))
#	fp.write('^\n')	
fp.close()