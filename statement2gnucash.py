import sys

recipients_account = {
	'7-Eleven': 'Expenses:Grocery',
	'Alcatel-Lucent Deposit': 'Expenses:Other',
	'Alcatel-Lucent Salary': 'Income',
	'Apartment Rent': 'Expenses:Utilities',
	'Amazon': 'Expenses:Other',
	'Amazon Video on Demand': 'Expenses:Leisure',
	'Amazon Local': 'Expenses:Leisure',
	'Apple Online Store': 'Expenses:Other',
	'Bank Account Fee': 'Expenses:Utilities',
	'Beverage': 'Expenses:Grocery',
	'Book(s)': 'Expenses:Leisure',
	'CalTrain Ticket': 'Expenses:Transportation',
	'Car Oil Change': 'Expenses:Transportation',
	'Chocolate': 'Expenses:Grocery',
	'Cinema': 'Expenses:Leisure',
	'Comcast Internet': 'Expenses:Utilities',
	'Computer History Museum': 'Expenses:Leisure',
	'Credit Card Payment': 'Credit Card:HSBC Credit Card',
	'Dentist': 'Expenses:Medical Care',
	'Dollar Tree': 'Expenses:Other',
	'Dozens': 'Assets:HSBC:Savings Account',
	"Felipe's Market": 'Expenses:Grocery',
	'From dozens': 'Assets:HSBC:Savings Account',
	'Gasoline': 'Expenses:Transportation',
	'Gift': 'Expenses:Other',
	'Google Shopping': 'Expenses:Grocery',
	'H&M': 'Expenses:Other',
	'H&R Block Tax Software': 'Expenses:Other',
	'HSBC Rewards Credit': 'Income',
	'Hotel': 'Expenses:Leisure',
	'IKEA': 'Expenses:Other',
	'In-N-Out Burger': 'Expenses:Grocery',
	'Insurance Auto @ Hartford': 'Expenses:Transportation',
	'Interest': 'Income',
	'Kaiser': 'Expenses:Medical Care',
	'Kickstarter': 'Expenses:Other',
	'Kiddie Academy Salary': 'Income',
	"Kohl's": 'Expenses:Other',
	'Leisure': 'Expenses:Leisure',
	'Lunch': 'Expenses:Grocery',
	'Marshalls': 'Expenses:Other',
	'Michaels': 'Expenses:Other',
	'Mission San Juan Bautista': 'Expenses:Leisure',
	'Mission Carmel': 'Expenses:Leisure',
	'Money sent to account in Slovenska Sporitelna': 'Equity:Opening Balances',
	'Money Transfer Fee': 'Expenses:Utilities',
	'Mountain View Educational Foundation (MVEF)': 'Expenses:Education',
	'Nob Hill': 'Expenses:Grocery',
	'Parking Fee': 'Expenses:Transportation',
	'PayPal': 'Expenses:Other',
	'PayPal Deposit': 'Expenses:Other',
	'PG&E electricity': 'Expenses:Utilities',
	'Photo': 'Expenses:Other',
	'Postal Services': 'Expenses:Other',
	'Precision Auto Protection': 'Expenses:Transportation',
	'Safeway': 'Expenses:Grocery',
	'Shoes': 'Expenses:Other',
	'State Railroad Museum Sacramento': 'Expenses:Leisure',
	'Subway': 'Expenses:Grocery',
	'Target': 'Expenses:Grocery',
	'T-Mobile refill account': 'Expenses:Utilities',
	'VTA Light Rail': 'Expenses:Transportation',
	'Walgreens': 'Expenses:Other',
	'Walmart': 'Expenses:Grocery',
	'Whole Food': 'Expenses:Grocery',
}

known_recipients = {
	'AMAZON MKTPLACE PMTS AMZN.COM/BILL WA': 'Amazon',
	'AMAZON.COM AMZN.COM/BILL WA': 'Amazon',
	'AMAZON VIDEO ON DEMAND 866-216-1072 WA': 'Amazon Video on Demand',
	'AMAZONLOCAL WWW.AMAZONLOC WA': 'Amazon Local',
	'PAYMENT TO PAYPAL-INST XFER': 'PayPal',
	'DEPOSIT FROM PAYPAL-TRANSFER': 'PayPal Deposit',
	'PAYMENT TO PGANDE-WEB ONLINE': 'PG&E electricity',
	'DEPOSIT FROM ALCATEL-LUCENT U-DIRECT DEP': 'Alcatel-Lucent Salary',
	'ATM OR OTHER ELECTRONIC BANKING TRANSACTION': 'Kiddie Academy Salary',
	'570 NORTH SHORELINEMOUNTAIN VIEWCA': 'Safeway',
	'645 SAN ANTONIO ROAD': 'Safeway',
	'1750 MIRAMONTEMOUNTAIN VIEWCA': 'Safeway',
	'PAYMENT TO COMCAST-COMCAST': 'Comcast Internet',
	'TARGET T0322 MOUNTAIN VMOUNTAIN VIEWCA': 'Target',
	'PAYMENT TO CENTRAL PARK AT-WEB PMTS': 'Apartment Rent',
	'199 E MIDDLEFIELD RDMOUNTAIN VIEWCA': 'Dollar Tree',
	'121 E EL CAMINO REALMOUNTAIN VIEWCA': 'Walgreens',
	'276 N WHISMAN AVEMOUNTAIN VIEWCA': '7-Eleven',
	'1101 W. EL CAMINO REALSUNNYVALE CA': "Felipe's Market",
	"FELIPE'S MARKET SUNNYVALE CA": "Felipe's Market",
	'600 SHOWERS DRIVEMOUNTAIN VIE CA': 'Walmart',
	'600 SHOWERS DRIVEMOUNTAIN VIEWCA': 'Walmart',
	'2280 WAL-SAMS': 'Walmart',
	'2280 WAL-SAMSMOUNTAIN VIEWCA': 'Walmart',
	'818 W EL CAMINO REALSUNNYVALE CA': 'Michaels',
	'PAYMENT - THANK YOU': 'Credit Card Payment',
	'LE BOULANGER - MOUNTAIMOUNTAIN VIEW CA': 'Lunch',
	'CALTRAIN TVMSAN CARLOS CA': 'CalTrain Ticket',
	'CALTRAIN TVM SAN CARLOS CA': 'CalTrain Ticket',
	'VTA LIGHT RAIL TVM SAN JOSE CA': 'VTA Light Rail',
	'PRECISION AUTO PROTECT 888-965-1591 FL': 'Precision Auto Protection',
	'SOLACE 888-965-1591 FL': 'Precision Auto Protection',
	'SOLACE CLEARWATER FL': 'Precision Auto Protection',
	'A1 AUTO TECH INC MOUNTAIN VIEW CA': 'Car Oil Change',
	'THERESA FRANK DDS SUNNYVALE CA': 'Dentist',
	'STATEMENT REWARDS CREDIT': 'HSBC Rewards Credit',
	'IKEA EAST PALO ALTO E PALO ALTO CA': 'IKEA',
	'KICKSTARTER.COM INTERNET GB': 'Kickstarter',
	'AMC CUPERTINO SQ #0424 CUPERTINO CA': 'Cinema',
	'LIFETOUCH NSS ONLINE P 800-736-4753 MN': 'Photo',
	'PASSAGE TO INDIA MOUNTAIN VIEW CA': 'Lunch',
	'CARMEL MISSION MUSEUM CARMEL CA': 'Mission Carmel',
	'IN *CARMEL MISSION MUS 831-6241271 CA': 'Mission Carmel',
	'OLD MISSION SAN JUAN SAN JUAN BAUT CA': 'Mission San Juan Bautista',
	'APL*APPLEONLINESTOREUS 800-676-2775 CA': 'Apple Online Store',
	'APL* ITUNES.COM/BILL 866-712-7753 CA': 'Apple Online Store',
	'645 SAN ANTONIO ROADMOUNTAIN VIEWCA': 'Safeway',
	'COMPUTER HISTORY MMOUNTAIN VIEW CA': 'Computer History Museum',
	'BLK*MTN VIEW EDUC FDN 6505263500 CA': 'Mountain View Educational Foundation (MVEF)',
	'CA STATE RAILROAD MUSE SACRAMENTO CA': 'State Railroad Museum Sacramento',
	'MOUNTAIN VIEW COAST MOUNTAIN VIEW CA': 'Gasoline',
	'Money sent to account in Slovenska Sporitelna': 'Money sent to account in Slovenska Sporitelna',
	'BIBIMBOWLSUNNYVALE CA': 'Lunch',
	'ZABB CUISINESAN FRANCISCO CA': 'Lunch',
	'DEPOSIT FROM ALLUIDOC-PAYMENTS': 'Alcatel-Lucent Deposit',
	'H&R BLOCK TAX SOFTWARE 8004725625 MO': 'H&R Block Tax Software',
}

known_recipients_partial = {
	'AMAZON': 'Amazon',
	'KAISER ': 'Kaiser',
	'ONLINE TRANSFER TO SAVINGS 00876462654': 'Dozens',
	'ONLINE TRANSFER FROM SAVINGS 00876462654': 'From dozens',
	'ONLINE PAYMENT TO ACCOUNT 04656209739': 'Credit Card Payment',
	'PAYMENT TO THE HARTFORD-NTPLAGYCOL': 'Insurance Auto @ Hartford',
	'T-MOBILE': 'T-Mobile refill account',
	'SUBWAY ': 'Subway',
	'IN-N-OUT BURGER': 'In-N-Out Burger',
	'INTEREST EARNED AND PAID FROM': 'Interest',
	'NOB HILL': 'Nob Hill',
	'SAFEWAY': 'Safeway',
	'WAL-MART': 'Walmart',
	'TARGET': 'Target',
	'CHEVRON ': 'Gasoline',
	'ROTTEN ROBBIE ': 'Gasoline',
	'GAS': 'Gasoline',
	'OIL': 'Gasoline',
	'ARCO': 'Gasoline',
	"KOHL'S": "Kohl's",
	"KOHLS": "Kohl's",
	'MARSHALLS': 'Marshalls',
	'FUNDS TRANSFER OUTGOING FEE': 'Money Transfer Fee',
	'RESTAURANT ': 'Lunch',
	'CAFE': 'Lunch',
	'GHIRARDELLI': 'Chocolate',
	'PARKIN': 'Parking Fee',
	'USPS': 'Postal Services',
	'GOOGLE ': 'Google Shopping',
	'GIFT': 'Gift',
	'7-ELEVEN': '7-Eleven',
	'ARAMARK SYMANTEC MOUNT': 'Lunch',
	'PIZZA': 'Lunch',
	'Hotel': 'Hotel',
	'Lunch': 'Lunch',
	'SUSHI': 'Lunch',
	'Leisure:': 'Leisure',
	"MCDONALD'S": 'Lunch',
	'DINING': 'Lunch',
	'FOOD': 'Lunch',
	'DONUTS': 'Lunch',
	'BEVER': 'Lunch',
	"CARL'S JR": 'Lunch',
	'BIER': 'Beverage',
	'FAMOUS FOOTWEA': 'Shoes',
	'H&M': 'H&M',
	'PIZZERIA': 'Lunch',
	'SERVICE CHG* BASED ON': 'Bank Account Fee',
	'WHOLEFDS': 'Whole Food',
	'GOURMET': 'Lunch',
	'DOLRTREE': 'Dollar Tree',
	'199 E MIDDLEFIELD RD': 'Dollar Tree',
	'SCHOLASTIC BOOK FAIR': 'Book(s)',
	'PAYLESS SHOESOURCE': 'Shoes',
}

lines = open(sys.argv[1]).readlines()
statement = []
for line in lines:
	line = line.strip()
	if line == '' or line.startswith('#'): continue
	line = line.replace('\t', ' ')
	if len(line.split(' ')[0].split('/')) == 3:
		statement.append(line)
	#elif len(statement) > 0:
	#	statement[-1] += line
	
transactions = []
for line in statement:	
	t_category = t_date = t_description = t_account = t_number = t_amount = ''
	
	#remove redundant spaces
	a = []
	for b in line.split(' '):
		if b.strip() != '': a.append(b)

	if len(a[0].split('/')) == 3 and len(a[1].split('/')) == 3:
		#credit card
		t_category = 'Credit Card:HSBC Credit Card'
		t_date = a[0]
		t_description = ' '.join(a[2:-2]).strip()
		t_amount = a[-1]
		if t_amount.startswith('$-'):
			continue
	elif len(a[0].split('/')) == 3:
		#accounts
		t_category = 'Assets:HSBC:Checking Account'
		t_date = a[0]
		if a[-2].find('.') != -1:
			t_description = a[1:-2]
			t_amount = a[-2]
		else:
			t_description = a[1:-1]
			t_amount = a[-1]
		if t_description[0] == 'PURCHASE':
			b = t_description[1:]
			if b[0] == 'MADE': b.pop(0)
			b.pop(0)
			if b[0].find('/'): t_date = b[0] + '/' + t_date.split('/')[2]
			t_description = b[2:]
		elif t_description[0] == 'CHECK':
			t_number = t_description[1]
			if t_amount == '40.00':
				t_description = ['Donations in church']				
				t_account = 'Charity'
			elif float(t_amount.replace(',', '')) > 3050.00 and float(t_amount.replace(',', '')) < 3130.00:
			  t_description = ['Apartment Rent']
			  t_account = recipients_account[t_description[0]]
		t_description = ' '.join(t_description)
	else:
	    print "warning: Malformed line", line

	if known_recipients.has_key(t_description):
		t_account = recipients_account[known_recipients[t_description]]
		t_description = known_recipients[t_description]
	else:
		for key in known_recipients_partial.keys():
			if t_description.find(key) != -1:
				t_account = recipients_account[known_recipients_partial[key]]
				if t_description.startswith('Leisure: '):
					t_description = t_description[9:]
				elif t_description.startswith('Hotel '):
					pass
				else:
					t_description = known_recipients_partial[key]
				break
	if t_description == 'Kaiser' and t_amount == '$20.00':
		t_description = 'Doctor Appointment (Kaiser)'		
	elif t_description == 'PayPal' and t_amount == '$7.99':
		t_description = 'Netflix'
		t_account = 'Expenses:Utilities'
	
	t_date_iso = '20%(y)02d-%(m)02d-%(d)02d' % (dict(zip(['m', 'd', 'y'], [int(c) for c in t_date.split('/')])))
	t_date = '%(m)02d/%(d)02d/20%(y)02d' % (dict(zip(['m', 'd', 'y'], [int(c) for c in t_date.split('/')])))
	t_amount = t_amount.replace(',', '').replace('.', ',').replace('$', '').strip()	
	
	if t_account == '':
		print t_date_iso, t_description, t_amount
		t_account = 'Expenses:Other'
		t_description = '????? '+t_description
		
	if t_account != 'Income' and t_description not in ['Alcatel-Lucent Deposit', 'PayPal Deposit']:
	  if t_amount.startswith('-'):
	    t_amount = t_amount[1:]
	  else:
		  t_amount = '-'+t_amount
	elif t_amount.startswith('-'):
	  t_amount = t_amount[1:]
	transactions.append([t_date_iso, t_account, t_description, t_amount, t_number])	

transactions.sort()
fp = open(sys.argv[1].split('.')[0]+'.qif', 'w')
fp.write('!Account\nN%s\nTBank\n^\n!Type:Bank\n' % (t_category))
for t in transactions:
	fp.write('D%s\nL%s\nP%s\nT%s\n' % (t[0], t[1], t[2], t[3]))
	if t[4] != '':
		fp.write('N%s\n' % (t[4]))
	fp.write('^\n')	
fp.close()