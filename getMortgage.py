import glob, os, sys, subprocess

pdftotext = 'C:\\Tools\\xpdfbin\\bin32\\pdftotext.exe'
tmpfile = 'tmpfile.txt'

if len(sys.argv) == 1:
    print('error: No directory with statements specified')
    sys.exit(1)

try:
	fout = open('mortgage.xls', 'w')
	fout.write('datum\turok\tistina\tsplatka\tpoplatok\n')
except:
	print("error: Failed to open output file")
	sys.exit(1)

for fn in glob.glob(sys.argv[1]+'/*.pdf'):
	if subprocess.call([pdftotext, '-table', '-nopgbrk', fn, tmpfile]) != 0:
		print("error: Failed to execute "+pdftotext)
		sys.exit(1)

	try:
		fp = open(tmpfile)
		lines = fp.readlines()
		fp.close()
	except:
		print("error: Failed to open file %s (%s)" % (tmpfile, fn))
		sys.exit(1)
	os.unlink(tmpfile)
		
	data = {}
	out = ''
	for line in lines:
		line = line.strip()
		if line.find('158705') != -1 and line.find('SPL.'):
			ar = line.split(' ')
			if line.find('SPL.UR.') != -1:
				data['urok'] = ar[-1]
			elif line.find('SPL.UVERU') != -1:
			    data['istina'] = ar[-1]
			try:
				d = ar[0].split('-')
				data['datum'] = '20%s-%s-%s' % (d[2], d[1], d[0])
			except:
			    data['datum'] = ar[0]
		elif line.find('POPLATOK') != -1 and line.find('VEDENIE') != -1 and line.find('UCTU') != -1:
			data['poplatok'] = line.split(' ')[-1]

	if len(data) > 0:
		for par in ['urok', 'istina', 'poplatok']:
			val = data[par]
			if val.startswith('-'):
				val = val[1:]
			elif val.endswith('-'):
				val = val[:-1]
			data[par] = val
		data['splatka'] = float(data['urok']) + float(data['istina'])
		out = '%(datum)s\t%(urok)s\t%(istina)s\t%(splatka)0.2f\t%(poplatok)s' % (data)
		print(out)
		out = out.replace('.', ',')	
		fout.write(out+'\n')

fout.close()